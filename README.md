# web

`static website code for antifa.tech`

[antifa.tech](https://antifa.tech) is a yellow-pages-like website to track services and collective which run and mantain antifa services online.

# design

## logos antifa.tech

- ![antifa-tech-logo](static/assets/img/antifa-tech.svg)
- ![antifa-tech-icon](static/assets/img/antifa-tech-icon.svg)

## cards icons
FontAwesome icons loaded locally, no external references.

Select icons available here:
 https://fontawesome.com/v5.15/icons?d=gallery&m=free
 https://fontawesome.com/icons?d=gallery&m=free


# contribute

if you want to help feel free to contact-us

1. drop a msg to info<at>antifa<dot>tech
2. add an issue to discuss about new services and collectives
3. create a pull-request to propose a change

## edit information

### services
Edit `data/service.json` or check for services already listed.
Choose categories from this file when you describe your server.

### servers
Copy and edit `data/servers/YOURNAME.md` to submit your server info, you will have to add at least:

`vim data/servers/YOURNAME.md` to submit your server info, inside you have to add at least:


```
{
    "name": "YOUR PUBLIC NAME",
    "url": "https://YOURDOMAIN.tdl",
    "description": "YOUR SHORT DESCRIPTION",
    "services": {
	"service": "https://SERVICE.YOURSERVER.TDL",
	"anotherservice": "https://ANOTHERSERVICE.YOURSERVER.TDL"
    }
}
```

if you want to give a deeper information use the next data set including the parameters you will like to show:

```
{
    "name": "YOUR PUBLIC NAME",
    "url": "https://YOURDOMAIN.tdl",
    "description": "YOUR SHORT DESCRIPTION",
    "serverlogo": "https://YOURSERVER.TDL/YOURLOGO.svg",
    "languages": {
	lang-ca: "false",
	lang-de: "false",
	lang-en: "false",
	lang-es: "false",
	lang-fr: "false",
	lang-gl: "false",
	lang-it: "true",
	lang-pt: "false"
    }
    "country": "no",
    "region": "Border/Continent",
    "services": {
	"service": "https://SERVICE.YOURSERVER.TDL",
	"anotherservice": "https://ANOTHERSERVICE.YOURSERVER.TDL"
    }
}
```

## Generating the web: hugo

### Developing and previewing changes
Clone this repo, move inside the directory and type:
```
hugo -D --bind localhost -p 1313 server -b http://localhost/
```

then visit [](http://localhost:1313/) in the local machine

### Deploying
When ready type `hugo` and check the content of `public/` and publish or use it as you want.

